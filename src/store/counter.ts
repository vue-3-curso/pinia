
import {defineStore} from 'pinia'

//Por convención de nombres usamos la misma mecánica usada en los hooks : use+nombre+Store
export const useCounterStore=defineStore('counter',{
    state:()=>{
        return{
            count:1
        }
    },
    getters:{
        times2:(state)=>state.count*2
    },
    actions:{
        increment(val=1){
            this.count+=1
        }
    }
})